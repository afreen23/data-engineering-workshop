This folder should contain both your rendered (.pdf) slides as well as the source files you used to generate the slides, including any images or videos.

The Google Slides version of a template slide deck can be found [here](https://drive.google.com/file/d/1xNWa3_TuRPoq7bgDLMQFwtQ8zRgjcQQL/view?usp=sharing). This is a good starting point for your presentation, and contains advice on style and structure as well as guidance on what NOT to put in your slide decks!
