# Deployment Instructions

This workshop  is deployed using the OpenDatahub Operator found [here](https://gitlab.com/opendatahub/opendatahub-operator).

## Instructions to deploy

1. Clone the opendatahub-operator repository.
2. Copy `workshop.yaml` to the root of the opendatahub-operator repository.
3. Follow the instructions for installation as found at [manual-installation](https://gitlab.com/opendatahub/opendatahub-operator/blob/master/docs/manual-installation.adoc).
4. To Deploy Ceph, follow the instructions under `Ceph installation with the Rook operator` in the [reference architecture](https://opendatahub.io/arch.html)

## RHPDS link 
You can use [RHPDS](https://rhpds.redhat.com/catalog/explorer) (Red Hat Product Demo system) to create the OpenShift environment to setup the workshop.

## Instructions to deploy on RHPDS
In the `Catalogs` page, look for the `All Services` -> `Workshops` -> `OpenShift 4.2 Workshop` service. After that, follow the workshop setup instructions in this [link](../doc/setup.md)

## Instructions to deploy outside of RHPDS
You can use Code Ready Containers to deploy your environment. Install CRC as per [instructions](https://access.redhat.com/documentation/en-us/red_hat_codeready_containers/1.0/html/getting_started_guide/getting-started-with-codeready-containers_gsg#installing-codeready-containers_gsg) and then follow the workshop setup instructions in this [link](../doc/setup.md)