import boto3
import pandas as pd
from io import StringIO
import os

endpoint = os.environ['ENDPOINT']
access_key = os.environ['ACCESS_KEY']
secret_key = os.environ['SECRET_KEY']
region = os.environ['REGION']
bucket = os.environ['BUCKET']
source_path = os.environ['SOURCE_PATH']
destination_path = os.environ['DESTINATION_PATH']


def connect_to_s3(endpoint, access_key, secret_key, region, use_ssl=True, verify=None):
    client = boto3.client('s3',
                          endpoint_url=endpoint,
                          aws_access_key_id=access_key,
                          aws_secret_access_key=secret_key,
                          region_name=region,
                          use_ssl=use_ssl,
                          verify=verify)
    return client

connection = connect_to_s3(endpoint, access_key, secret_key, region)
resp = connection.list_objects_v2(Bucket=bucket, Prefix=source_path)
keys = []
i = 0
for obj in resp['Contents']:
    keys.append(obj['Key'])
    i += 1
    if i == 10:
        break

dfs = []
for key in keys:
    dfs.append(pd.read_csv(StringIO(connection.get_object(Bucket=bucket,Key=key)['Body'].read().decode('utf-8'))))

final = pd.concat(dfs)

csv_buf = StringIO()
final.to_csv(csv_buf, header=True, index=False)
csv_buf.seek(0)
connection.put_object(Bucket=bucket, Body=csv_buf.getvalue(), Key=destination_path)
