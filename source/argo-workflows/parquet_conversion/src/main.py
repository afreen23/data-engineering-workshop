import boto3
import pandas as pd
from io import BytesIO
import os

endpoint = os.environ['ENDPOINT']
access_key = os.environ['ACCESS_KEY']
secret_key = os.environ['SECRET_KEY']
region = os.environ['REGION']
bucket = os.environ['BUCKET']
source_path = os.environ['SOURCE_PATH']
destination_path = os.environ['DESTINATION_PATH']


def connect_to_s3(endpoint, access_key, secret_key, region, use_ssl=True, verify=None):
    client = boto3.client('s3',
                          endpoint_url=endpoint,
                          aws_access_key_id=access_key,
                          aws_secret_access_key=secret_key,
                          region_name=region,
                          use_ssl=use_ssl,
                          verify=verify)
    return client

connection = connect_to_s3(endpoint, access_key, secret_key, region)
obj = connection.get_object(Bucket=bucket, Key=source_path)
df = pd.read_csv(obj['Body'])

parquet_buf = BytesIO()
df.to_parquet(parquet_buf)
parquet_buf.seek(0)
connection.put_object(Bucket=bucket, Body=parquet_buf.getvalue(), Key=destination_path)
