# Find the Hard Drive Serial Numbers with no failures
SELECT serial_number, sum(failure)
FROM harddrives.hd_data
GROUP BY serial_number
HAVING sum(failure) == 0

# Find the total capacity (in Terabytes) of backblaze datacenter by date
SELECT date, sum(capacity_bytes)/1099511627776 AS capacity_tbytes
FROM harddrives.hd_data
GROUP BY date
ORDER BY date

# Find the total capacity (in Terabytes) of backblaze datacenter by model ordered by total amount
SELECT model, sum(capacity_bytes)/1099511627776 AS capacity_tbytes
FROM harddrives.hd_data
GROUP BY model
ORDER BY sum(capacity_bytes) DESC

# Find the amount of Hard Drives per model ordered by the amount limited by the top 10
SELECT model, count(serial_number)
FROM harddrives.hd_data
GROUP BY model
ORDER BY count(serial_number) DESC
LIMIT 10

# Find the number of failures per day
SELECT date, sum(failure)
FROM harddrives.hd_data
GROUP BY date

