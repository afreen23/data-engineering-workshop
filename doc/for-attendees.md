# Open Data Hub Basic tutorial

This tutorial is intended as part of an interactive in-person workshop and will show users how to use the Open Data Hub and Ceph to curate data using Data Catalog features in Open Data Hub.

## Prerequisites

The following has been provided for you as prerequisites for the workshop tutorial:

* A running OpenShift cluster.
* An installed Open Data Hub operator in the OpenShift cluster.
* A project space in OpenShift for deploying and managing Open Data Hub.

## Deploying Open Data Hub to Your OpenShift Project Space

In order to get started with the Open Data Hub, you need to deploy it into your project namespace.

1. Go to the OpenShift console login page provided on your handout.

2. Enter the Attendee ID and Password on your handout provided during the workshop to log into OpenShift.

3. After logging in, go to your list of projects by clicking on `Home -> Projects`.  Click on the project with your Attendee ID to open that project namespace. Ex. user250.  This will take you to the project status page.

4. Now you can deploy the Open Data Hub into your project.  On the left side menu, go to `Operators -> Installed Operators`.  This will display a list of installed operators to deploy. Click on `Open Data Hub` under `Provided APIs`column.
 ![installed-operators](../images/installed-operators.png)

5. The Open Data Hub operator page appears. Click on the blue `Create Open Data Hub` button.

6. The Open Data Hub Custom Resource (CR for short) will be displayed. The CR is used to configure the deployment of the ODH that the operator will spin up. For the purposes of the workshop, you must configure the CR exactly as the [template](../doc/templates/workshop.yaml) provided for this workshop. Click on `Create` to deploy the Open Data Hub components and wait until the deployment is complete.

**NOTE:** You must change the `aws_access_key_id`, `aws_secret_access_key` and `s3_endpoint` parameters with the values of the Ceph S3 credentials you received.

7. It will take a while for all of the Open Data Hub components to start up.  To ensure all pods of the Open Data Hub are deployed successfully, click on `Workloads -> Pods` on the left menu. Wait until all pods should show `Ready` in the Readiness column. This should take about 5 minutes to complete.
![pods-ready](../images/pods-ready.png)

8. If you want to get an overview of the different pods, services, and routes that the Operator deploys, you can go to `Catalog -> Installed Operators`, and then click on `Open Data Hub`. Under `Open Data Hub`, you can find and click on `example-opendatahub` and view the individual bits and pieces of the deployment under the `resources` tab.

9. Now you can start data engineering work in Open data Hub. The next section will show how to upload data into Ceph S3.

## Uploading data into Ceph 

1. We will use the [Hard Drive data and stats](https://f001.backblazeb2.com/file/Backblaze-Hard-Drive-Data/data_Q3_2019.zip) dataset from [Backblaze](https://www.backblaze.com/) to create our tables. In case of lack of resources there is a [small version](../source/dataset/hard-drive.zip) of the dataset. Download the file and unzip to your home folder.

2. Set the environment variables with the values of the Ceph S3 location and credentials you received.
```bash
S3_HOSTNAME=<hostname>
S3_ACCESS_KEY=<access_key>
S3_SECRET_KEY=<secret_key>
```

3. Now let's configure s3cmd to connect to the Ceph S3 storage. If you don't have s3cmd installed, follow the [installation instructions](https://s3tools.org/s3cmd) to install.
```bash
s3cmd \
    --configure \
    --no-ssl \
    --host=$S3_HOSTNAME \
    --access_key=$S3_ACCESS_KEY \
    --secret_key=$S3_SECRET_KEY
```

**NOTE1:** The wizard will ask you the following question:
```bash
DNS-style bucket+hostname:port template for accessing a bucket [rook-ceph-rgw-my-store-rook-ceph.apps.cluster-gru-769e.gru-769e.example.opentlc.com]: 
```
Make sure the hostname is the same as the Ceph hostname.
**NOTE2:** In case you don't have Python installed in your laptop or you don't want to install, you can ask the instructor and he will guide you to open a terminal in JupyterHub to run the commands.

4. Create the bucket to store the data.
```bash
s3cmd mb s3://workshop
```

5. With the created bucket we can upload the data. Run the following command
```bash
s3cmd put \
    ~/data_Q3_2019/data_Q3_2019/ \
    --recursive \
    --acl-public \
    --encoding=UTF-8 \
    s3://workshop/hard-drive/
```

6. Check if all files were uploaded. You should have 92 files stored in the bucket.
```bash
s3cmd ls s3://workshop/hard-drive/ | wc -l
```

## Preprocess datasets with Argo Workflows

1. Argo provides a way for you to run a series of steps (known as Workflows) on a given schedule. This can be especially powerful for data engineers who need to perform regular transformations on data.

2. Tying it back to the dataset, lets create an Argo Workflow that will transform the vast number of csv files we have into a consolidated Parquet file. Parquet is a far more efficient way of storing data, so transforming the dataset will allow us to save space as well speed up any processing.

3. Open the [workflow](../source/argo-workflows/data-transformation-workflow.yaml)

4. Populate the following values for the first step in the workflow. You will use the same S3 Credentials as you have used prior to this point. For Source Path, you would put in `workshop/hard-drive`, whereas your destination path can be whatever you want.

```yaml
      - name: ENDPOINT
        value: <insert_endpoint_here>
      - name: ACCESS_KEY
        value: <insert_key_here>
      - name: SECRET_KEY
        value: <insert_secret_key_here>
```

5. Populate the following values for the second step in the workflow. You will use the same S3 Credentials as you have used prior to this point.

6. Save the file after making the changes, and then in the `Dashboard` page click on the plus(+) sign on the top-right corner close to the username text.
![Argo - Add YAML](../images/argo-add-yaml.png)

7. Paste the YAML file contents in the textarea with the changed values and click on `Create` button
![Argo - Add YAML](../images/argo-yaml.png)

8. Find the Argo UI URL by clicking on `argo-ui` deployment and in the `Resources` tab in the right pane. You will find the URL in the `Routes` section.
![Argo route](../images/argo-route.png)

9. You will see a new argo workflow pop up in the argo ui, as well as some new pods in the OpenShift interface.
![Argo UI](../images/argo-ui.png)

10. Clicking on the workflow and you can see the workflow details.
![Argo Workflow details](../images/argo-workflow.png)

11. And clicking in the steps you can find details of each step.
![Argo Step details](../images/argo-step.png)

12. If the work ran successfuly you will see all steps green.
![Argo Workflow execution success](../images/argo-success.png)

13. Now, if you look at the files created by the workflows, you will notice a decrease in total size.
```bash
s3cmd ls -H s3://workshop/hard-drive-preprocessing/
s3cmd ls -H s3://workshop/hard-drive-parquet/
```

## Catalogging data sets with Hive Metastore and Hue

1. Find the hue URL by clicking on the hue deployment and in the`Resources` tab in the right pane. You will find the URL in the `Routes` section.
![routes](../images/hue-route.png)

2. Hue will ask you to create an admin user when you access for the first time. 
![user creation](../images/hue-user-creation.png)

3. Create the user `hue` with password `hue` and after that login with the new user.
![login](../images/hue-login.png)

4. The Hue editor will appear in a blank textarea.
![editor](../images/hue-editor.png)

5. Copy the contents of [hard-drives.sql](../source/data-model/hard-drives.sql) file into Hue Editor to create the `hardrives` database and the `hd_data` table. You will create a table based on the parquet files stored in Ceph.
![sql](../images/hue-sql.png)

6. just for testing purposes, see if you can access the data by running `SELECT * FROM harddrives.hd_data LIMIT 50` query in Hue editor.
![sql](../images/hue-query-results.png)

7. We have the data model to explore the data we have. The next section will ask some questions which you need to prepare a SQL query to answer.

## Data exploration

1. Now that we have the table, we will explore the data. Answer the questions below with a SQL query:

  - Find the Hard Drive Serial Numbers with no failures

  - Find the total capacity of backblaze datacenter capacity by date

  - Find the total capacity (in Terabytes) of backblaze datacenter by model ordered by total amount

  - Find the amount of Hard Drives per model ordered by the amount limited by the top 10

  - Find the number of failures per day

2. With all these questions answered, let's create a dashboard that can give us a visual analysis of these queries.

**NOTE:** For the curious or for the unexperienced SQL developers the answer for these questions can be found [here](../source/queries/answer.sql).

## Running Jupyter Notebooks in OpenShift

1. Once you've opened JupyterHub from the link, you'll notice that you don't have SSL certificates properly set up for certificate authorization.  This will cause a warning to be displayed in your browser. You will need to use Private Browsing in Safari or Incognito Mode in Chrome, or click on the `Proceed` link.  If you are using FireFox, you can use the default mode but you will need to add a security exception by going to on `Advanced -> Add Exception... -> Confirm Security Exception` when the security warning displays in the browser. To log in, you will use the same credentials that you used to log into the OpenShift cluster.

2. When accessing JupyterHub for the first time after getting past the security warning, it prompts the following:

            Authorize Access
            Service account jupyterhub-hub in project opendatahub-user14 is requesting permission to access your account (user14)
            Requested permissions
            user:info
    This is just Jupyter Hub's authentication system asking for permission to get access to the user:info data. The user needs to select "Allow selected permissions"

3. JupyterHub is used in the Open Data Hub for spawning Jupyter notebook servers with pre-installed tools for creating AI and machine learning models. For more information on Jupyter notebooks, visit [https://jupyter.org/](https://jupyter.org/). Once logged in, click on `Start my Server`. You will be redirected to a page with a number of prefilled/selected fields. You don't need to change any settings -- just hit the `Spawn button`. This may take a few moments to complete and you will be redirected to a Jupyter notebook server. The default settings are as follows.
![defaults](../images/defaults.png)

4. Open the `data_analysis.ipynb` notebook by clicking on it. It can be found under [source/notebooks](../source/notebooks). This will open the notebook in Jupyter. The notebook will contain the rest of the instructions for the tutorial.

5. Notebooks contain *cells*.  Run each cell in your notebook by clicking on the `>| Run` button or hitting `Shift+Enter` for each cell, starting with the first.  When a cell is actively running, it will have `[*]` on the side.  Some cells will take time to run.  Once done, it will have a number, such as `[1]`.  You can go through the entire notebook until all cells are run.

## Visualizing Data with Superset

1. Find the hue URL by clicking on the superset deployment and in the `Resources` tab in the right pane. You will find the URL in the `Routes` section.

2. Login to superset with user `admin` and password `workshop`.
![Superset login](../images/superset-login.png)

3. In the superset main page, go to the `Sources -> Databases` menu.
![Superset - Databases configuration](../images/superset-databases.png)

4. In the Databases page, click on the plus sign to add a new record.
![Superset - New Database](../images/superset-new-database.png)

5. In the new Database page, fill the `Database` field with value `Open Data Hub Thrift Server` and in the `SQLAlchemy URI` field add the value `hive://<thrift-server-ip>:10000/harddrives`. You can find the Thrift Server Service IP by going to the OpenShift dashboard, select `Networking -> Services` and find the `thrift-server` service.
![superset - Database details](../images/superset-database-details.png)

6. Click on the `Test Connection` button. If your configuration is correct, you should see a popup with the `Seems OK!` message. After that, close the popup window and Click on the `Save` button in the bottom of the page.
![superset - Database connection successful](../images/superset-database-success.png)

7. Now let's create the table record so superset can create visualizations with the table we created in Hue. go to the `Sources -> Tables` menu.
![superset - Database connection successful](../images/superset-table.png)

8. In the Tables page, click on the plus sign to add a new record.
![superset - Database connection successful](../images/superset-new-table.png)

9. In the new Table page, select the new database we created in the `Database` field and fill the `Table` field with value `hd_data`.
![superset - Database connection successful](../images/superset-table-details.png)

10. Click on the `Save` button in the bottom of the page.
![superset - Database connection successful](../images/superset-table-created.png)

11. In this point, you are free to explore superset in order to convert the queries you created in the last section into Charts and then put everything in a dashboard. We'll help you creating your first chart and then it's your turn to explore your creativity.

**NOTE:** For the impacients, there is a ready-to-use dashboard in this [link](../source/superset-dashboard/20200115_234955.json).